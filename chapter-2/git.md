# Git

Git is a version control system (VCS) for tracking changes in computer files and coordinating work on those files among multiple people. It is primarily used for software development, but it can be used to keep track of changes in any files.

Let's initialize git in our current project

```bash
~/w/hello ❯❯❯ git init
Initialized empty Git repository in /Users/karim/wad/hello/.git/
```
You need to initialize git only once for any project. As we can see that a `.git` directory has been created inside our project. All the git data is stored inside this directory.

As long as you are the only developer working on a project, you need to remember only three basic git commands:
1. git add
2. git commit
3. git push

This will stage all the modified files, add a commit message and push the changes to github.

##### Step 1. Add
``` bash
~/w/hello ❯❯❯ git add .
```
##### Step 2. Commit
~/w/hello ❯❯❯ git commit -m "Initial commit"
[master (root-commit) 5c65bd3] Initial commit
 3 files changed, 28 insertions(+)
 create mode 100644 Gemfile
 create mode 100644 Gemfile.lock
 create mode 100644 app.rb

##### Step 3. Push
``` bash
~/w/hello ❯❯❯ git push origin master                                                                        master
fatal: 'origin' does not appear to be a git repository
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
```

git fails here because it does not know where to push! Go to github.com and create a new project named `hello`.
After creating the project add a remote inside your project. You need to configure this only once.

```bash
git remote add origin git@github.com:techmaniack/hello.git
git push -u origin master
Counting objects: 5, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (5/5), 573 bytes | 0 bytes/s, done.
Total 5 (delta 0), reused 0 (delta 0)
To github.com:techmaniack/hello.git
 * [new branch]      master -> master
Branch master set up to track remote branch master from origin.
```
---
https://en.wikipedia.org/wiki/Git
https://help.github.com/articles/adding-a-new-ssh-key-to-your-github-account/
