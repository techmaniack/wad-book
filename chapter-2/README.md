# Hello Sinatra

**Sinatra** is a DSL for quickly creating web applications in Ruby with minimal effort.

##### Objectives
- Get started with Sinatra by creating a hello world application. 
- Understanding use of rubygems and bundler.
- Git fundamentals.

---
##### Working Directory
``` bash
~/wad/hello
```
##### Requirements
- ruby 2.4
- bundler gem (gem install bundler)
- git (sudo apt-get install git)

---
### Resources
- http://www.sinatrarb.com/documentation.html
- http://www.sinatrarb.com/intro.html
