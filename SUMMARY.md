# Summary

* [Introduction](README.md)
* [1. Prep](chapter-1/README.md)
  * [Ruby installation](chapter-1/ruby_installation.md)
  * [Linux commands](chapter-1/linux_commands.md)

* [2. Hello Sinatra](chapter-2/README.md)
  * [Gemfile](chapter-2/gemfile.md)
  * [Git Basics](chapter-2/git.md)
  * [Params](chapter-2/params.md)

* [3. TDD ](chapter-3/README.md)
* [4. Using Database](chapter-4/README.md)
* [5. Deploying to the Cloud](chapter-5/README.md)
